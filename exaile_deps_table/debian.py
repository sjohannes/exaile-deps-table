# Copyright 2020, 2022 Johannes Sasongko <sasongko@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from collections import defaultdict
from html import escape, unescape
import re
import threading
import traceback
from typing import cast, DefaultDict, Dict, List, Match, Optional, Tuple

from .common import Distro, distro, DistroPackages, opener, urlquote


DebianVersions = DefaultDict[Tuple[str, str], List[str]]

DEBIAN_COLOR = (0xA8, 0x00, 0x30)

debian_lock = threading.Lock()
debian_cache: Dict[str, Optional[DebianVersions]] = {}


def get_versions(src: str, arch: str) -> Optional[DebianVersions]:
    with debian_lock:
        try:
            return debian_cache[src]
        except KeyError:
            debian_cache[src] = versions = _get_versions(src, arch)
            return versions


def _get_versions(src: str, arch: str) -> Optional[DebianVersions]:
    versions: Optional[DebianVersions]
    try:
        with opener.open(
            f'https://qa.debian.org/madison.php?package={urlquote(src)}&table=all&a={arch}&c=&s=&S=on&text=on'
        ) as f:
            versions = defaultdict(list)
            for line in f:
                bin, ver, distro, _arch = map(str.strip, line.decode('ascii').split(' | '))
                m = re.search(r'([^-].+?)-', ver)
                if m:
                    ver = m[1]
                versions[(bin, distro)].append(ver)
    except Exception:
        traceback.print_exc()
        versions = None
    return versions


@distro("debian")
def process_debian() -> Distro:
    try:
        with opener.open('https://www.debian.org/releases/stable/') as f:
            html = f.read()
        m = re.search(br'Debian &ldquo;(.+?)&rdquo;', html)
        m = cast(Match[bytes], m)
        codename_h = m[1].decode('ascii')
        codename = unescape(codename_h)
        m = re.search(br'Debian (\d+)', html)
        m = cast(Match[bytes], m)
        release_h = m[1].decode('ascii')
    except Exception:
        traceback.print_exc()
        return Distro("Debian stable (not found)", color=DEBIAN_COLOR)

    def get_version(src: str, bin: str, arch: str = 'amd64') -> str:
        srcvers = get_versions(src, arch)
        versions = srcvers[(src if arch == 'source' else bin, codename)] if srcvers else None
        version = ", ".join(versions) if versions else "not found"
        return (
            f"{escape(version)} "
            f"<div class=pkg>(<a href='https://packages.debian.org/stable/{escape(urlquote(bin))}'>{escape(bin)}</a>)</div>"
        )

    return Distro(
        f"Debian ({release_h} = {codename_h})",
        DistroPackages(
            glib=get_version('glib2.0', 'libglib2.0-0'),
            gstreamer=get_version('gstreamer1.0', 'libgstreamer1.0-0'),
            gtk=get_version('gtk+3.0', 'libgtk-3-0'),
            mutagen=get_version('mutagen', 'python3-mutagen', 'source'),
            pygobject=get_version('pygobject', 'python3-gi'),
            python=get_version('python3-defaults', 'python3'),
        ),
        color=DEBIAN_COLOR,
        logo="debian",
    )


@distro("debian01")
def process_debian_testing() -> Distro:
    try:
        with opener.open('https://www.debian.org/releases/testing/') as f:
            html = f.read()
        m = re.search(br'Debian &ldquo;(.+?)&rdquo;', html)
        m = cast(Match[bytes], m)
        codename_h = m[1].decode('ascii')
        codename = unescape(codename_h)
    except Exception:
        traceback.print_exc()
        return Distro("Debian testing (not found)", color=DEBIAN_COLOR)

    def get_version(src: str, bin: str, arch: str = 'amd64') -> str:
        srcvers = get_versions(src, arch)
        versions = srcvers[(src if arch == 'source' else bin, codename)] if srcvers else None
        version = ", ".join(versions) if versions else "not found"
        return (
            f"{escape(version)} "
            f"<div class=pkg>(<a href='https://packages.debian.org/testing/{escape(urlquote(bin))}'>{escape(bin)}</a>)</div>"
        )

    return Distro(
        f"Debian: testing ({codename_h})",
        DistroPackages(
            glib=get_version('glib2.0', 'libglib2.0-0t64'),
            gstreamer=get_version('gstreamer1.0', 'libgstreamer1.0-0'),
            gtk=get_version('gtk+3.0', 'libgtk-3-0t64'),
            mutagen=get_version('mutagen', 'python3-mutagen', 'source'),
            pygobject=get_version('pygobject', 'python3-gi'),
            python=get_version('python3-defaults', 'python3'),
        ),
        color=DEBIAN_COLOR,
        logo="debian",
    )


@distro("debian99")
def process_debian_unstable() -> Distro:
    codename = 'sid'

    def get_version(src: str, bin: str, arch: str = 'amd64') -> str:
        srcvers = get_versions(src, arch)
        versions = srcvers[(src if arch == 'source' else bin, codename)] if srcvers else None
        version = ", ".join(versions) if versions else "not found"
        return (
            f"{escape(version)} "
            f"<div class=pkg>(<a href='https://packages.debian.org/unstable/{escape(urlquote(bin))}'>{escape(bin)}</a>)</div>"
        )

    return Distro(
        f"Debian: unstable ({codename})",
        DistroPackages(
            glib=get_version('glib2.0', 'libglib2.0-0t64'),
            gstreamer=get_version('gstreamer1.0', 'libgstreamer1.0-0'),
            gtk=get_version('gtk+3.0', 'libgtk-3-0t64'),
            mutagen=get_version('mutagen', 'python3-mutagen', 'source'),
            pygobject=get_version('pygobject', 'python3-gi'),
            python=get_version('python3-defaults', 'python3'),
        ),
        color=DEBIAN_COLOR,
        logo="debian",
    )


@distro("linuxmint")
def process_mint() -> Distro:
    color = (0xBF, 0xFF, 0x80)
    try:
        with opener.open('https://linuxmint.com/download_all.php') as f:
            html = f.read()
        m = re.search(
            br'<h2>Linux Mint Releases</h2>.*?<table\b.*?<td\b.*?>([\d.]+?)</td>.*?<td\b.*?>Ubuntu (.*?)</td>',
            html,
            re.DOTALL,
        )
        m = cast(Match[bytes], m)
        release_h = m[1].decode('ascii')
        codename_h = m[2].lower().decode('ascii')
        codename = unescape(codename_h)
    except Exception:
        traceback.print_exc()
        return Distro("Linux Mint (not found)", color=color)

    def get_version(src: str, bin: str, arch: str = 'amd64', repo: Optional[str] = None) -> str:
        srcvers = get_versions(src, arch)
        key = (
            src if arch == 'source' else bin,
            f'ubuntu/{codename}/{repo}' if repo else f'ubuntu/{codename}',
        )
        versions = srcvers[key] if srcvers else None
        version = ", ".join(versions) if versions else "not found"
        return (
            f"{version} "
            f"<div class=pkg>(<a href='https://packages.ubuntu.com/{escape(urlquote(codename))}/{escape(urlquote(bin))}'>{bin}</a>)</div>"
        )

    return Distro(
        f"Linux Mint ({release_h} &larr; Ubuntu {codename_h})",
        DistroPackages(
            glib=get_version('glib2.0', 'libglib2.0-0t64'),
            gstreamer=get_version('gstreamer1.0', 'libgstreamer1.0-0'),
            gtk=get_version('gtk+3.0', 'libgtk-3-0t64'),
            mutagen=get_version('mutagen', 'python3-mutagen', 'source', 'universe'),
            pygobject=get_version('pygobject', 'python3-gi'),
            python=get_version('python3-defaults', 'python3'),
        ),
        color=color,
        logo="linuxmint",
    )
