#!/usr/bin/env python3

# Copyright 2020-2021 Johannes Sasongko <sasongko@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from concurrent.futures import ThreadPoolExecutor
from typing import Tuple

from .common import DistroFunc, processors, table_format

# These are imported for side effects.
from . import alma, debian, fedora, msys2, others


N_THREADS = 16

HTML_PREFIX = """<!doctype html>
<html lang=en>
<meta charset=utf-8>

<title>Exaile dependency tracking</title>

<link rel=icon href='data:,'>
<meta name=viewport content='width=device-width,initial-scale=1'>
<style>
html { background-color: #2a2a2e; color: #fff; font-family: sans-serif }
a { color: #39f; text-decoration: none }
a:hover { text-decoration: underline }
table { background-color: #000; display: block; overflow-x: auto }
thead, tbody { display: block }
tr {
 background-color: #1a1a1a;
 border: 1px solid #777;
 box-sizing: border-box;
 display: grid;
 grid-template-columns: calc(16px + 0.3rem) repeat(7, minmax(5em, 1fr));
 min-width: calc(4px + 6 * 5em);
 /*padding-left: 4px;*/
}
thead tr { background-color: #444 }
tbody tr { border-top: none }
th, td { display: block; padding: 0.3rem; overflow-wrap: break-word }
th:nth-child(2) { text-align: left }
th:nth-child(9) { display: none }
td:nth-child(9) { grid-column: 2/-1; grid-row: 2 }
.ver { text-align: center }
.pkg { font-size: 0.7rem }
.note { font-size: 0.75rem }
.note::before { content: "Note: " }
</style>

<h1>Exaile dependency versions in various OS repositories</h1>

<table>

<thead>
<tr>
<th></th>
<th>Repository</th>
<th>GLib</th>
<th>GStreamer</th>
<th>GTK</th>
<th>Mutagen</th>
<th>PyGObject</th>
<th>Python</th>
<th>Note</th>
</tr>
</thead>

<tbody>
"""

HTML_SUFFIX = """</tbody>

</table>

<p>For comparison: <a href='https://github.com/exaile/exaile/blob/master/DEPS'>Exaile DEPS</a>.</p>

<p>
Note:
When multiple Python versions are available, PyGObject binaries are usually only built for one of them.
On source-based systems, the PyGObject package may or may not be compatible with all available Python versions.
</p>

<hr>

<p>Generated with <a href='https://gitlab.com/sjohannes/exaile-deps-table'>exaile-deps-table</a>.</p>
"""


def main():
    from sys import argv, exit, stdout, stderr
    import time

    if len(argv) > 1:

        def execute(processor: Tuple[str, DistroFunc]) -> Tuple[str, str, float]:
            func = processor[1]
            t0 = time.time()
            result = func()
            t1 = time.time()
            return func.__name__, table_format(result), t1 - t0

        timings = []
        dists = argv[1:]
        if dists == ['all']:
            f = stdout
            f.write(f"{HTML_PREFIX}\n")
            with ThreadPoolExecutor(N_THREADS) as executor:
                for funcname, result, dt in executor.map(execute, sorted(processors.values())):
                    f.write(f"{result}\n")
                    timings.append((funcname, dt))
            f.write(HTML_SUFFIX)
            f.write(
                f"<p>Last updated {time.strftime('%Y-%m-%d %H:%M:%S UTC', time.gmtime())}.</p>\n"
            )
        else:
            f = stdout
            f.write(f"{HTML_PREFIX}\n")
            for dist in dists:
                funcname, result, dt = execute(processors[dist])
                f.write(f"{result}\n")
                timings.append((funcname, dt))
            f.write(HTML_SUFFIX)

        for funcname, dt in timings:
            print(f"{funcname}: {dt:.3f} s", file=stderr)

    else:
        print(f"Syntax: {argv[0]} all|{'|'.join(sorted(processors.keys()))}", file=stderr)
        exit(1)
