# Copyright 2020-2021 Johannes Sasongko <sasongko@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import base64
from pathlib import Path
from typing import Callable, Dict, NamedTuple, Optional, Tuple
from urllib.parse import quote as _orig_urlquote
from urllib.request import build_opener


class DistroPackages(NamedTuple):
    glib: str = ""
    gstreamer: str = ""
    gtk: str = ""
    mutagen: str = ""
    pygobject: str = ""
    python: str = ""


class Distro(NamedTuple):
    name: str
    packages: Optional[DistroPackages] = None
    note: str = ""
    color: Optional[Tuple[int, int, int]] = None
    logo: Optional[str] = None


DistroFunc = Callable[[], Distro]


opener = build_opener()
opener.addheaders = [('User-Agent', 'exaile-deps-table/5')]

processors: Dict[str, Tuple[str, DistroFunc]] = {}

root_dir = Path(__file__).parent.parent


def distro(sortkey: str) -> Callable[[DistroFunc], DistroFunc]:
    def wrapper(f: DistroFunc) -> DistroFunc:
        assert f.__name__.startswith('process_')
        processors[f.__name__[len('process_') :]] = (sortkey, f)
        return f

    return wrapper


def table_format(distro: Distro) -> str:
    # if distro.color:
    #     r, g, b = distro.color
    #     mark = f"#{r:02x}{g:02x}{b:02x}"
    #     color = f" style='border-left: 4px solid {mark}; padding-left: 0'"
    # else:
    #     color = ""
    color = ""
    packages = (
        "\n".join(f"<td class=ver>{p}</td>" for p in distro.packages)
        if distro.packages
        else "<td></td>" * len(DistroPackages._fields)
    )
    note = f"<td class=note>{distro.note}</td>\n" if distro.note else ""
    if distro.logo:
        img = (root_dir / "logos" / f"{distro.logo}.webp").read_bytes()
        img_b64 = base64.b64encode(img).decode("ascii")
        logo = f"<img src='data:image/webp;base64,{img_b64}' alt=''> "
    else:
        logo = ""
    return f"<tr{color}>\n<td>{logo}</td><td>{distro.name}</td>\n{packages}\n{note}</tr>\n"


def urlquote(s: str) -> str:
    return _orig_urlquote(s, safe='')
