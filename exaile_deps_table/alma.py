# Copyright 2020, 2022 Johannes Sasongko <sasongko@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from collections import defaultdict
from html import escape, unescape
import re
import traceback
from typing import DefaultDict, Pattern, Set, Tuple, Union
from urllib.parse import unquote as urlunquote

from .common import Distro, distro, DistroPackages, opener


AlmaVersions = DefaultDict[str, Set[Tuple[str, str]]]


def add_versions(packages: AlmaVersions, url: str) -> None:
    with opener.open(url) as f:
        html = f.read()
    for m in re.finditer(
        br'<a href="([^>"]+?)-(\d[^>"-]*?)(?:-[^>"]+?)?\.(?:module_)?el\d[^>"]*?\.(?:x86_64|noarch)\.rpm">',
        html,
    ):
        packages[m[1].decode('ascii')].add((m[2].decode('ascii'), url))


def format_versions(packages: AlmaVersions, package: Union[str, Pattern[str]]) -> str:
    if isinstance(package, re.Pattern):
        versions = [(p, vs) for p, vs in packages.items() if package.search(p)]
    else:
        versions = [(package, packages[package])]
    return " ".join(
        f"{escape(ver)} <div class=pkg>(<a href='{escape(url)}'>{escape(package)}</a>)</div>"
        for package, vers in versions
        for ver, url in vers
    )


@distro("almalinux")
def process_alma() -> Distro:
    color = (0x00, 0x69, 0xDA)
    try:
        with opener.open("https://mirrors.almalinux.org/isos.html") as f:
            html = f.read()
        release_tuple = max(
            rel.split(".")
            for rel in (
                urlunquote(unescape(m[1].decode("ascii")))
                for m in re.finditer(br'<a href="/isos/x86_64/([^"]+?)\.html"', html)
            )
            if re.match(r"^[\d.]+$", rel)
        )
        release = ".".join(release_tuple)
        packages: AlmaVersions = defaultdict(set)
        for repo in ['AppStream', 'BaseOS']:  # , 'extras'
            url = f'https://repo.almalinux.org/almalinux/{release}/{repo}/x86_64/os/Packages/'
            add_versions(packages, url)
    except Exception:
        traceback.print_exc()
        return Distro("AlmaLinux (not found)", color=color)

    return Distro(
        f"AlmaLinux ({release})",
        DistroPackages(
            glib=format_versions(packages, 'glib2'),
            gstreamer=format_versions(packages, 'gstreamer1'),
            gtk=format_versions(packages, 'gtk3'),
            mutagen="&mdash;",
            pygobject=format_versions(packages, 'python3-gobject'),
            python=format_versions(packages, 'python3'),
        ),
        color=color,
        logo="alma",
    )
