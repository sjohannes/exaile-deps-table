# Copyright 2020 Johannes Sasongko <sasongko@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from collections import defaultdict
from html import escape, unescape
import json
import re
import traceback
from typing import cast, DefaultDict, Dict, List, Match, Optional, Pattern, Tuple, Union

from .common import Distro, distro, DistroPackages, opener, urlquote


@distro("archlinux")
def process_arch() -> Distro:
    def get_version(repo: str, package: str, arch: str = 'x86_64') -> str:
        url = f'https://archlinux.org/packages/{urlquote(repo)}/{arch}/{urlquote(package)}/'
        try:
            with opener.open(f'{url}json/') as f:
                jsobj = json.load(f)
            result = (
                f"{escape(jsobj['pkgver'])} "
                f"<div class=pkg>(<a href='{escape(url)}'>{escape(package)}</a>)</div>"
            )
        except Exception:
            traceback.print_exc()
            result = (
                f"<div class=pkg>(<a href='{escape(url)}'>{escape(package)}</a> not found)</div>"
            )
        return result

    return Distro(
        "Arch Linux",
        DistroPackages(
            glib=get_version('core', 'glib2'),
            gstreamer=get_version('extra', 'gstreamer'),
            gtk=get_version('extra', 'gtk3'),
            mutagen=get_version('extra', 'python-mutagen', 'any'),
            pygobject=get_version('extra', 'python-gobject'),
            python=get_version('core', 'python'),
        ),
        color=(0x17, 0x93, 0xD1),
        logo="arch",
    )


@distro("freebsd")
def process_freebsd() -> Distro:
    def get_version_simple(cat: str, src: str, pkg: str) -> str:
        try:
            with opener.open(
                f'https://raw.githubusercontent.com/freebsd/freebsd-ports/main/{urlquote(cat)}/{urlquote(src)}/Makefile'
            ) as f:
                text = f.read()
            m = re.search(br'^\s*(?:PORT|DIST)VERSION\s*=\s*(.*?)\s*(#.*)?$', text, re.MULTILINE)
            m = cast(Match[bytes], m)
            version = m[1].decode('ascii')
        except Exception:
            traceback.print_exc()
            version = "not found"
        return (
            f"{escape(version)} "
            "<div class=pkg>("
            f"<a href='https://github.com/freebsd/freebsd-ports/tree/main/{escape(urlquote(cat))}/{escape(urlquote(src))}'>{escape(pkg)}</a>"
            ")</div>"
        )

    try:
        with opener.open(
            'https://raw.githubusercontent.com/freebsd/freebsd-ports/main/Mk/bsd.default-versions.mk'
        ) as f:
            text = f.read()
        m = re.search(br'^\s*PYTHON_DEFAULT\s*\??=\s*(.*)\s*$', text, re.MULTILINE)
        m = cast(Match[bytes], m)
        python_version = m[1].decode('ascii')
        python_shortver = python_version.replace('.', '')
    except Exception:
        traceback.print_exc()
        python_version = "not found"
        python_shortver = "??"
    else:
        try:
            with opener.open(
                f'https://raw.githubusercontent.com/freebsd/freebsd-ports/main/lang/python{python_shortver}/Makefile.version'
            ) as f:
                text = f.read()
            m = re.search(br'^\s*PYTHON_(?:PORT|DIST)VERSION\s*=\s*(.*)\s*$', text, re.MULTILINE)
            m = cast(Match[bytes], m)
            python_version = m[1].decode('ascii')
        except Exception:
            traceback.print_exc()
            python_version = f"{python_version}.?"
        python_version = (
            f"{python_version} "
            "<div class=pkg>("
            f"<a href='https://github.com/freebsd/freebsd-ports/tree/main/lang/python{python_shortver}'>python{python_shortver}</a>"
            ")</div>"
        )

    try:
        with opener.open(
            'https://raw.githubusercontent.com/freebsd/freebsd-ports/main/Mk/Uses/gstreamer.mk'
        ) as f:
            text = f.read()
        m = re.search(br'^\s*_GST1_VERSION\s*\??=\s*(.*)\s*$', text, re.MULTILINE)
        m = cast(Match[bytes], m)
        gstreamer_version = m[1].decode('ascii')
    except Exception:
        traceback.print_exc()
        gstreamer_version = "not found"
    else:
        gstreamer_version = (
            f"{gstreamer_version} "
            "<div class=pkg>("
            f"<a href='https://github.com/freebsd/freebsd-ports/tree/main/multimedia/gstreamer1'>gstreamer1</a>"
            ")</div>"
        )

    return Distro(
        "FreeBSD: ports",
        DistroPackages(
            glib=get_version_simple('devel', 'glib20', 'glib'),
            gstreamer=gstreamer_version,
            gtk=get_version_simple('x11-toolkits', 'gtk30', 'gtk3'),
            mutagen=get_version_simple('audio', 'py-mutagen', f'py{python_shortver}-mutagen'),
            pygobject=get_version_simple('devel', 'py-pygobject', f'py{python_shortver}-gobject3'),
            python=python_version,
        ),
        "Only <a href='https://raw.githubusercontent.com/freebsd/freebsd-ports/main/Mk/bsd.default-versions.mk'><var>PYTHON_DEFAULT</var></a> is listed here; other Python versions may be available.",
        color=(0xEB, 0x00, 0x28),
        logo="freebsd",
    )


@distro("gentoo")
def process_gentoo() -> Distro:
    def get_version(cat: str, package: str, prefix: str = '') -> str:
        url = f'https://packages.gentoo.org/packages/{urlquote(cat)}/{urlquote(package)}'

        try:
            with opener.open(url) as f:
                html = f.read().decode('utf-8')
        except Exception:
            traceback.print_exc()
            version = "not found"
        else:
            versions: Dict[str, str] = {}
            testing_marker = "<sup>~</sup>"
            for m in re.finditer(
                r'kk-slot.+?:\s*([^<]+?)\s*<.+?"([^ ]+?)(?:-r\d+|_p\d+)? +is (stable|testing) on amd64"',
                html,
                re.DOTALL,
            ):
                # For each slot, take the latest version, preferring stable
                slot = m[1]
                ver = m[2]
                if not ver.startswith(prefix):
                    continue
                testing = m[3] == 'testing'
                if testing:
                    ver += testing_marker
                vers = versions.get(slot)
                if vers is None:
                    versions[slot] = ver
                elif vers.endswith(testing_marker) and not testing:
                    versions[slot] = ver
            version = ", <br>".join(versions.values()) or "not found"

        return f"{version} <div class=pkg>(<a href='{escape(url)}'>{escape(cat)}/{escape(package)}</a>)</div>"

    return Distro(
        "Gentoo Linux",
        DistroPackages(
            glib=get_version('dev-libs', 'glib', '2.'),
            gstreamer=get_version('media-libs', 'gstreamer', '1.'),
            gtk=get_version('x11-libs', 'gtk+', '3.'),
            mutagen=get_version('media-libs', 'mutagen'),
            pygobject=get_version('dev-python', 'pygobject', '3.'),
            python=get_version('dev-lang', 'python', '3.'),
        ),
        color=(0x54, 0x48, 0x7A),
        logo="gentoo",
    )


@distro("openbsd")
def process_openbsd() -> Distro:
    color = (0xFF, 0xF4, 0x66)

    try:
        with opener.open('https://www.openbsd.org/') as f:
            html = f.read()
        m = re.search(br'>OpenBSD (\d+[.\d]*)</', html)
        m = cast(Match[bytes], m)
        release = m[1].decode('ascii')
        directory = f'https://cdn.openbsd.org/pub/OpenBSD/{release}/packages/amd64/'
        with opener.open(f'{directory}SHA256') as f:
            hashes = f.read()
    except Exception:
        traceback.print_exc()
        return Distro(
            "OpenBSD (not found)",
            color=color,
            logo="openbsd",
        )

    versions: DefaultDict[str, List[str]] = defaultdict(list)
    for m in re.finditer(br'^[^ ]+? \(([^)]+)-([^-)]+?)(?:p\d+)?\.\w+\)', hashes, re.MULTILINE):
        package_h = m[1].decode('ascii')
        version_h = m[2].decode('ascii')
        versions[package_h].append(version_h)
    directory_h = escape(directory)

    def get_version(package: str, version_h_filter: Optional[Pattern[str]] = None) -> str:
        package_h = escape(package)
        versions_h_unfiltered = versions.get(package_h)
        if versions_h_unfiltered is None:
            return "not found"
        versions_h: List[str]
        if version_h_filter:
            versions_h = []
            for version_h in versions_h_unfiltered:
                if version_h_filter.search(version_h):
                    versions_h.append(version_h)
        else:
            versions_h = versions_h_unfiltered
        return f"{', <br>'.join(versions_h)} <div class=pkg>(<a href='{directory_h}'>{package_h}</a>)</div>"

    return Distro(
        f"OpenBSD ({release})",
        DistroPackages(
            glib=get_version('glib2'),
            gstreamer=get_version('gstreamer1'),
            gtk=get_version('gtk+3'),
            mutagen=get_version('py3-mutagen'),
            pygobject=get_version('py3-gobject3'),
            python=get_version('python', re.compile(r'^3\.')),
        ),
        color=color,
        logo="openbsd",
    )


@distro("pkgsrc")
def process_pkgsrc() -> Distro:
    versions: Dict[str, Tuple[str, str, str]] = {}
    try:
        with opener.open('http://cdn.netbsd.org/pub/pkgsrc/current/pkgsrc/README-all.html') as f:
            html = f.read()
    except Exception:
        traceback.print_exc()
    else:
        for m in re.finditer(br'<a href="([^>]+?)">([^<]+?)-([^<-]+?)(?:nb\d+)?</a>', html):
            url_qh = m[1].decode('ascii')
            url_qh = f'https://ftp.netbsd.org/pub/pkgsrc/current/pkgsrc/{url_qh}'
            package_h = m[2].decode('ascii')
            version_h = m[3].decode('ascii')
            versions[unescape(package_h)] = (package_h, version_h, url_qh)

    def format_version(package_h: str, version_h: str, url_qh: str) -> str:
        return f"{version_h} <div class=pkg>(<a href='{url_qh}'>{package_h}</a>)</div>"

    def get_version(package: Union[str, Pattern[str]]) -> str:
        if isinstance(package, re.Pattern):
            version = " ".join(
                format_version(*v) for k, v in reversed(versions.items()) if package.search(k)
            )
            if not version:
                version = "not found"
        else:
            v = versions.get(package)
            if v is None:
                version = "not found"
            else:
                version = format_version(*v)
        return version

    return Distro(
        "pkgsrc (current)",
        DistroPackages(
            glib=get_version('glib2'),
            gstreamer=get_version('gstreamer1'),
            gtk=get_version('gtk3+'),
            mutagen=get_version(re.compile(r'^py3\d+-mutagen$')),
            pygobject=get_version(re.compile(r'^py3\d+-gobject3$')),
            python=get_version(re.compile(r'^python3\d+$')),
        ),
        color=(0xF4, 0x85, 0x40),
        logo="pkgsrc",
    )
