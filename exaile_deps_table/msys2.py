# Copyright 2020, 2022 Johannes Sasongko <sasongko@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import contextlib
from html import escape
import shutil
import subprocess
import tarfile
import tempfile
import traceback
from typing import Dict, IO, Optional, Tuple

from .common import Distro, distro, DistroPackages, opener, urlquote


MIRROR = 'http://mirror.msys2.org'


def parse_descfile(f: IO[bytes]) -> Optional[Tuple[str, str, str]]:
    prev = name = base = version = None
    for line in f:
        if prev == b'%NAME%\n':
            name = line.rstrip().decode('ascii')
            prev = None
        elif prev == b'%BASE%\n':
            base = line.rstrip().decode('ascii')
            prev = None
        elif prev == b'%VERSION%\n':
            version = line.rstrip().decode('ascii')
            prev = None
        else:
            prev = line
    if name is None or version is None:
        return None
    if base is None:
        base = name
    return name, base, version


@distro("windows-msys2")
def process_msys2() -> Distro:
    packages: Dict[str, Tuple[str, str]] = {}

    try:
        with contextlib.ExitStack() as ctx:
            remote_f = ctx.enter_context(opener.open(f'{MIRROR}/mingw/x86_64/mingw64.db.tar.zst'))

            tmp_f = ctx.enter_context(tempfile.TemporaryFile())
            shutil.copyfileobj(remote_f, tmp_f, 2 * 1024 * 1024)
            tmp_f.seek(0)

            zstd_p = subprocess.Popen(["zstd", "-d"], stdin=tmp_f, stdout=subprocess.PIPE)

            tar = ctx.enter_context(tarfile.open(fileobj=zstd_p.stdout, mode='r|'))

            for info in tar:
                if not info.isfile() or not info.name.endswith('/desc'):
                    continue
                f = tar.extractfile(info)
                assert f
                t = parse_descfile(f)
                if t is None:
                    continue
                name, base, version = t
                pos = version.find('-')
                if pos != -1:
                    version = version[:pos]
                packages[name] = (base, version)
    except Exception:
        traceback.print_exc()

    def get_version(package: str) -> str:
        try:
            base, version = packages[package]
        except KeyError:
            traceback.print_exc()
            base, version = "??", "not found"
        return (
            f"{escape(version)} "
            f"<div class=pkg>(<a href='https://github.com/msys2/MINGW-packages/tree/master/{escape(urlquote(base))}'>{escape(package)}</a>)</div>"
        )

    return Distro(
        "Windows: MSYS2",
        DistroPackages(
            glib=get_version('mingw-w64-x86_64-glib2'),
            gstreamer=get_version('mingw-w64-x86_64-gstreamer'),
            gtk=get_version('mingw-w64-x86_64-gtk3'),
            mutagen=get_version('mingw-w64-x86_64-python-mutagen'),
            pygobject=get_version('mingw-w64-x86_64-python-gobject'),
            python=get_version('mingw-w64-x86_64-python'),
        ),
        "<a href='https://github.com/exaile/exaile-sdk-win/releases'>exaile-sdk-win</a> may be behind.",
        color=(0x89, 0x4C, 0x84),
        logo="msys2",
    )
