# Copyright 2020 Johannes Sasongko <sasongko@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from html import escape
import json
import re
import traceback
from typing import cast, Match

from .common import Distro, distro, DistroPackages, opener, urlquote


FEDORA_COLOR = (0x3C, 0x6E, 0xB4)


def get_version(release: str, package: str) -> str:
    url = f'https://mdapi.fedoraproject.org/{urlquote(release)}/pkg/{urlquote(package)}'
    try:
        with opener.open(url) as f:
            jsobj = json.load(f)
        version = jsobj['version']
    except Exception:
        traceback.print_exc()
        version = "not found"
    return (
        f"{escape(version)} "
        f"<div class=pkg>(<a href='{escape(url)}'>{escape(package)}</a>)</div>"
    )


@distro("fedora")
def process_fedora() -> Distro:
    try:
        with opener.open('https://fedoraproject.org/en/workstation/download/') as f:
            html = f.read()
        m = re.search(br"Fedora Workstation (\d+)", html)
        m = cast(Match[bytes], m)
        relnum = m[1].decode('ascii')
        release = f'f{relnum}'
    except Exception:
        traceback.print_exc()
        return Distro("Fedora Linux (not found)", color=FEDORA_COLOR)

    return Distro(
        f"Fedora Linux ({relnum})",
        DistroPackages(
            glib=get_version(release, 'glib2'),
            gstreamer=get_version(release, 'gstreamer1'),
            gtk=get_version(release, 'gtk3'),
            mutagen=get_version(release, 'python3-mutagen'),
            pygobject=get_version(release, 'python3-gobject'),
            python=get_version(release, 'python3'),
        ),
        color=FEDORA_COLOR,
        logo="fedora",
    )


@distro("fedora99")
def process_fedora_rawhide() -> Distro:
    return Distro(
        "Fedora Linux: rawhide",
        DistroPackages(
            glib=get_version('rawhide', 'glib2'),
            gstreamer=get_version('rawhide', 'gstreamer1'),
            gtk=get_version('rawhide', 'gtk3'),
            mutagen=get_version('rawhide', 'python3-mutagen'),
            pygobject=get_version('rawhide', 'python3-gobject'),
            python=get_version('rawhide', 'python3'),
        ),
        color=FEDORA_COLOR,
        logo="fedora",
    )
